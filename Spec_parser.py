
# coding: utf-8

# In[3]:

import os
import pandas as pd
import numpy as np
from matplotlib import pyplot as plt
import scipy as sp


# In[4]:

import SpecWork


# In[ ]:

class Spec_parser():
    
    def __init__(self, path):
        
        self.path = path
        lis = []
        for i in os.listdir(path):
            if '.txt' not in i:
                continue
            try:
                time = SpecWork.Specwork(path + '/' + i)
                time.name = i
                lis.append(path + '/' + i)
            except:
                continue
        self.lis = lis
        
    def lookspec(self):
        
        anomals = []
        for i in self.lis:
            time = SpecWork.Specwork(i)
            print(i[:-4])
            time.disp()
            inl = input()
            if inl == 'end':
                break
            elif inl == 'ano':
                anomals.append(time.spectr)
            clear_output()
        return anomals
    
    def justX(self, baseline = 0, obrez = True, medfilt=0):
        
        f = 0
        for i in self.lis:
            time = SpecWork.Specwork(i)
            [m, n] = time.spectr.shape
            if n < 1024:
                continue
            if m >= 100:
                time = time.meanbysto()
            [m, n] = time.spectr.shape
            if obrez:
                time.obrez()
            #print(time.spectr.shape)
            if baseline:
                time.baseline()
            time.spectr *= (time.spectr >= 0)
            if medfilt:
                time.Nmedfilt(medfilt)
            if f == 0:
                zapis = time.spectr
                y = [time.label()]*m
                f = 1
                continue
            try:
                zapis = np.vstack((zapis, time.spectr))
            except:
                continue
            y.append(time.label())
        return zapis, np.array(y)
    
    def makexshort(self): # Создаёт матрицу объектов и вектор меток
        
        f = 0
        for i in self.lis:
            time = SpecWork.Specwork(i)
            [m, n] = time.spectr.shape
            if n < 1024:
                continue
            if m >= 100:
                time = time.meanbysto()
            [m, n] = time.spectr.shape
            time.obrez()
            #print(time.spectr.shape)
            time.baseline()
            time.spectr *= (time.spectr >= 0)
            time.Nmedfilt(4)
            if f == 0:
                zapis = time.spectr
                y = [time.label()]*m
                f = 1
                continue
            try:
                zapis = np.vstack((zapis, time.spectr))
            except:
                continue
            y.append(time.label())
        return zapis, np.array(y)



# coding: utf-8

# In[1]:

import numpy as np
from matplotlib import pyplot as plt


# In[2]:

class Specwork():
    
    def __init__(self, filename):
        
        spec = np.loadtxt(filename)
        self.name = filename
        self.abscissa = spec[:, 0]
        self.spectr = spec[:, 1:].T
        
    def ABS(self, obrez = True):
        
        if obrez:
            y = np.loadtxt('/home/bogdan/Рабочий стол/Prog_Classes/root/abscissa_short.txt')
            return y
        y = np.loadtxt('/home/bogdan/Рабочий стол/Prog_Classes/root/abscissa.txt')
        return y
        
    def label(self): # Присваивает значение имени белка
        
        name = self.name
      #  if ('Mg10' in name) or ('MG' in name) or ('ogl' in name):
      #      return 2
      #  elif ('HSA' in name) or ('lb' in name) or ('Alb' in name) and ('GHSA' not in name):
      #      return 3
      #  elif ('GHSA' in name):
      #      return 1
        if ('HSA' in name) and ('GHSA' not in name):
            return 3
        elif ('G' in name) or ('Gl' in name):
            return 1
        else:
            return 0

    def obrez(self):
        
        [m, n] = np.shape(self.spectr)
        #Obrez spectra sleva
        indexes = np.where(self.abscissa > 450)[0]
        self.abscissa = self.abscissa[indexes]
        self.spectr = self.spectr[:, indexes]
        #Obrez spectra sprava
        indexes = np.where(self.abscissa < 1800)[0]
        self.abscissa = self.abscissa[indexes]
        self.spectr = self.spectr[:, indexes]
        
        return self
    
    def baseline(self): # Vychet basovoy linii
        
        x = self.abscissa
        n = len(x)
        Y = self.spectr
        [m, n] = np.shape(Y)
        for i in range(int(m)):
            y = Y[i , :]
            if sum(np.isnan(y)) > 0:
                continue
            o = []
            for i in range(int(n/48)):
                time = y[48*i:48*(i+1)]
                a = (np.where(time == time.min())[0])
                #print(time)
                #print(np.where(time == time.min()))
                o.append(((a) + 48*i)[0])
            Y = np.vstack((Y, y - np.interp(x, x[o], y[o])))
                
        self.spectr = Y[m:, :]*(Y[m:, :] > 0)
        return self
    
    def meanbysto(self): # Усредняет серии экспериментов
        
        [m, n] = self.spectr.shape
        a = []
        for i in range(m//100):
            a.append(self.spectr[(i-1)*100:i*100, :].mean(0))
        self.spectr = np.array(a)
        return self
    
    def disp(self, cvet = None): # Postroenie graphika spectra
        
        x = self.abscissa
        #print(len(x))
        Y = self.spectr
        [m, n] = Y.shape
        #print(m, n)
        if m > 1:
            meana = Y.mean(0)
            if cvet:
                fig = plt.plot(x, meana, cvet)
            else:
                fig = plt.plot(x, meana, 'r')
            plt.ylim(min(meana), max(meana))
            plt.xlabel('1/cm')
            plt.show(fig)
        else:
            if cvet:
                fig = plt.plot(x, Y[0], cvet)
            else:
                fig = plt.plot(x, Y[0], 'r')
            plt.ylim(min(Y[0]), max(Y[0]))
            plt.xlabel('1/cm')
            plt.show(fig)
            
    def dispN(self, Numer, cvet = None): # Postroenie graphika spectra
        
        x = self.abscissa
        #print(len(x))
        Y = self.spectr
        [m, n] = Y.shape
        if m < Numer:
            print("It is not possible")
            return None
        #print(m, n)
        if cvet:
            fig = plt.plot(x, Y[Numer], cvet)
        else:
            fig = plt.plot(x, Y[Numer], 'r')
        plt.ylim(min(Y[Numer]), max(Y[Numer]))
        plt.xlabel('1/cm')
        plt.show(fig)
            
    def Nmedfilt(self, N): # Mediana po N tochek
        
        x = self.abscissa
        Y = self.spectr
        #type(Y)
        [m, n] = Y.shape
        N = int(N)
        newY = np.zeros(m)[np.newaxis].T
        #print(newY.shape)
        newx = np.zeros(1)
        
        i = 0
        while i < n/N:
            
            left = i*N
            right = (i + 2)*N
            medY = np.median(Y[:, left:right], 1)[np.newaxis].T
            #print(medY.shape)
            medx = np.median(x[left:right])
            newY = np.hstack((newY, medY))
            newx = np.hstack((newx, medx))
            
            i += 1
        self.abscissa = newx[1:]
        self.spectr = newY[:, 1:]
        
        return self
    
    def Nmeanfilt(self, N): # Srednee po N tochek
        
        x = self.abscissa
        Y = self.spectr
        #type(Y)
        [m, n] = Y.shape
        N = int(N)
        newY = np.zeros(m)[np.newaxis].T
        #print(newY.shape)
        newx = np.zeros(1)
        
        i = 0
        while i < n/N:
            
            left = i*N
            right = (i + 2)*N
            medY = np.mean(Y[:, left:right], 1)[np.newaxis].T
            #print(medY.shape)
            medx = np.mean(x[left:right])
            newY = np.hstack((newY, medY))
            newx = np.hstack((newx, medx))
            
            i += 1
        self.abscissa = newx[1:]
        self.spectr = newY[:, 1:]
        
        return self
    
    def Nmaxfilt(self, N): # Srednee po N tochek
        
        x = self.abscissa
        Y = self.spectr
        #type(Y)
        [m, n] = Y.shape
        N = int(N)
        newY = np.zeros(m)[np.newaxis].T
        #print(newY.shape)
        newx = np.zeros(1)
        
        i = 0
        while i < n/N:
            
            left = i*N
            right = (i + 2)*N
            medY = np.max(Y[:, left:right], 1)[np.newaxis].T
            #print(medY.shape)
            medx = np.max(x[left:right])
            newY = np.hstack((newY, medY))
            newx = np.hstack((newx, medx))
            
            i += 1
        self.abscissa = newx[1:]
        self.spectr = newY[:, 1:]
        
        return self
    
    def peaks(self):
        
        lx = self.abscissa
        x = self.spectr
        [m, n] = np.shape(x)
        zer = np.zeros((m, 1))
        x_low = np.concatenate((zer, x[:, :-1]), axis = 1)
        x_hi = np.concatenate((x[:, 1:], zer), axis = 1)
        pik = ((x - x_low) > 0)*x
        pik = pik*((x - x_hi) > 0)
        x = np.transpose(x)
        pik = np.transpose(pik)
        me = np.mean(x)
        pik = (pik > me)*pik
        self.spectr = np.transpose(pik)
        #pik = np.vstack((lx, pik))
        return self


# In[ ]:



